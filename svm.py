#imports section

# pip install plotly if needed
import imp
import pandas
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn import svm
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import collections
import plotly
import plotly.plotly
import plotly.graph_objs

# to show plotly plots inside the jupyter notebook https://plot.ly/python/offline/
#init_notebook_mode(connected=True)

dataOpinions = datasets.load_files("data/txt_sentoken")

X = dataOpinions.data
Y = dataOpinions.target

# creating training and testing set
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, random_state=1)


from sklearn.feature_extraction.text import TfidfVectorizer




svm_pipeline = Pipeline(
        [
            ('dataVect', CountVectorizer()),
            ('tfidf', TfidfTransformer()),
            ('SVM', svm.SVC())
        ])

svm_parameters = {
    'tfidf__use_idf' : [True, False],
    'SVM__gamma' : [0.001],
    'SVM__probability': [True],
    'SVM__kernel': ['rbf','linear','poly'],
    'SVM__C' : [1, 1000]
}

svm_grid = GridSearchCV(svm_pipeline, svm_parameters, n_jobs=-1, return_train_score=True, cv=3)

print("On va fitter")
svm_fit = svm_grid.fit(X_train, Y_train)

print("On prédit")
svm_prediction = svm_grid.predict(X_test)


print("\nbest params:")
for param_name in sorted(svm_parameters.keys()):
    print("\t%s: %r" % (param_name, svm_grid.best_params_[param_name]))
